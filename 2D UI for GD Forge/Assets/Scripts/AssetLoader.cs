using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using System.Threading.Tasks;

public class AssetLoader : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private AssetReference shopItemPrefab;
    [SerializeField]
    private AssetReference imageListPrefab;
    async Task Start()
    {
        var prefabShopItem = await Addressables.LoadAssetAsync<GameObject>(shopItemPrefab).Task;
        var prefabImageList = await Addressables.LoadAssetAsync<GameObject>(imageListPrefab).Task;
        var imageList = prefabImageList.GetComponent<ImageList>().texturesList;
        for (int i = 0; i < imageList.Count; i++)
        {
            prefabShopItem.GetComponent<ShopItemComponent>().iconImage = imageList[i];
            Instantiate(prefabShopItem);
        }
    }
}
