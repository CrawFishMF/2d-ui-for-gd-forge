using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor;

public class ChatVisualizer : MonoBehaviour
{
    //We will be pulling in our SourceAsset from TitleScreenUI GameObject so we can reference Visual Elements
    [SerializeField]
    private UIDocument titleUIDocument;

    //When we grab the rootVisualElement of our UIDocument we will be able to query the TitleScreenManager Visual Element
    private VisualElement titleScreenManagerVE;

    //Within TitleScreenManager (which is everything) we will query for our shop-grid-box by name
    //We don't have to query for the TitleScreen THEN shop-grid-box because it is one big tree of elements
    //We can call any child from the parent, very convenient! But you must be mindful about being dilligent about
    //creating unique names or else you can get back several elements (which at times is the point of sharing a name)
    private VisualElement chatList;

    private GameObject chatHandler;
    private List<Message> chatListContainer;
    //This is our ChatList uxml that we will drag to the public field
    //We need a reference to the uxml so we can build it in makeItem
    [SerializeField]
    private VisualTreeAsset chatListOwn;
    [SerializeField]
    private VisualTreeAsset chatListEnemy;

    //These variables are used in Update() to pace how often we check for GameObjects

    [SerializeField]
    private float perSecond = 1.0f;
    private float nextTime = 0;

    void OnEnable()
    {
        //Here we grab the SourceAsset rootVisualElement
        //This is a MAJOR KEY, really couldn't find this key step in information online
        //If you want to reference your active UI in a script make a public UIDocument variable and 
        //then call rootVisualElement on it, from there you can query the Visual Element tree by names
        //or element types
        titleScreenManagerVE = titleUIDocument.rootVisualElement;
        //From within TitleScreenManager we query local-games-list by name
        chatList = titleScreenManagerVE.Q<VisualElement>("chat-list");

    }

    // Start is called before the first frame update
    void Start()
    {
        chatHandler = GameObject.FindGameObjectWithTag("ChatHandler");
        chatListContainer = chatHandler.GetComponent<MessageList>().messageList;

        CreateElements();
    }


    public new void SendMessage(string text)
    {
        //add new message to gameobj
        var ownMessage = new Message { isOwn = true, message = text };
        chatListContainer.Add(ownMessage);
        var enemyMessage = new Message { isOwn = false, message = "bib-boop" };
        chatListContainer.Add(enemyMessage);
        chatList.Clear();
        CreateElements();
    }

    private VisualElement MakeItemOwn()
    {
        //Here we take the uxml and make a VisualElement
        VisualElement listItem = chatListOwn.CloneTree();
        return listItem;

    }
    private VisualElement MakeItemEnemy()
    {
        //Here we take the uxml and make a VisualElement
        VisualElement listItem = chatListEnemy.CloneTree();
        return listItem;
    }

    private void BindItem(VisualElement e, string text)
    {
        //We add the game name to the label of the list item
        e.Q<Label>("message-text").text = text;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= nextTime)
        {
            chatList.Clear();
            CreateElements();

            //We increment
            nextTime += (1 / perSecond);
        }

    }

    private void CreateElements()
    {

        foreach (var msg in chatListContainer)
        {
            if (msg.isOwn)
            {
                var chatRow = MakeItemOwn();
                BindItem(chatRow, msg.message);
                chatList.Add(chatRow);
            }
            else
            {
                var chatRow = MakeItemEnemy();
                BindItem(chatRow, msg.message);
                chatList.Add(chatRow);
            }
        }
    }

}