using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor;

public class ItemsFinder : MonoBehaviour
{
    //We will be pulling in our SourceAsset from TitleScreenUI GameObject so we can reference Visual Elements
    public UIDocument titleUIDocument;

    //When we grab the rootVisualElement of our UIDocument we will be able to query the TitleScreenManager Visual Element
    private VisualElement titleScreenManagerVE;

    //Within TitleScreenManager (which is everything) we will query for our shop-grid-box by name
    //We don't have to query for the TitleScreen THEN shop-grid-box because it is one big tree of elements
    //We can call any child from the parent, very convenient! But you must be mindful about being dilligent about
    //creating unique names or else you can get back several elements (which at times is the point of sharing a name)
    private VisualElement gridBox;

    //Here we will store our discovered GameObjects
    //This will be used as our itemSource
    private GameObject[] discoveredShopItemObjects;

    //This is our GridItem uxml that we will drag to the public field
    //We need a reference to the uxml so we can build it in makeItem
    public VisualTreeAsset gridItemAsset;

    //These variables are used in Update() to pace how often we check for GameObjects
    public float perSecond = 1.0f;
    private float nextTime = 0;

    void OnEnable()
    {
        //Here we grab the SourceAsset rootVisualElement
        //This is a MAJOR KEY, really couldn't find this key step in information online
        //If you want to reference your active UI in a script make a public UIDocument variable and 
        //then call rootVisualElement on it, from there you can query the Visual Element tree by names
        //or element types
        titleScreenManagerVE = titleUIDocument.rootVisualElement;
        //link gridBox with shop-grid-box
        //From within TitleScreenManager we query shop-grid-box by name
        gridBox = titleScreenManagerVE.Q<VisualElement>("shop-grid-box");
    }

    // Start is called before the first frame update
    void Start()
    {
        //We start by looking for any GameObjects with a LocalGame tag
        discoveredShopItemObjects = GameObject.FindGameObjectsWithTag("ShopGridItem");

        CreateElements();
    }

    private VisualElement MakeItem()
    {
        //Here we take the uxml and make a VisualElement
        VisualElement GridItem = gridItemAsset.CloneTree();
        return GridItem;

    }

    private void BindItem(VisualElement e, int index)
    {
        //We add image of item to grid
        e.Q<VisualElement>("item-image").style.backgroundImage =
            discoveredShopItemObjects[index].GetComponent<ShopItemComponent>().iconImage;

        //Here we create a call back for clicking on the grid item and provide data to a function
        e.Q<Button>("buy-button").RegisterCallback<ClickEvent>(ev => ClickedBuyItem(discoveredShopItemObjects[index]));

    }

    void ClickedBuyItem(GameObject localGame)
    {
        Debug.Log("clicked 'BUY'");
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= nextTime)
        {
            //We check for GameObjects with a ShopGridItem tag
            discoveredShopItemObjects = GameObject.FindGameObjectsWithTag("ShopGridItem");

            //clearing container cause to avoid of duplicates
            gridBox.Clear();
            CreateElements();

            //We increment
            nextTime += (1 / perSecond);
        }

    }

    private void CreateElements()
    {
        for (int i = 0; i < discoveredShopItemObjects.Length; i++)
        {
            var gridTile = MakeItem();
            BindItem(gridTile, i);
            gridBox.Add(gridTile);
        }
    }
}