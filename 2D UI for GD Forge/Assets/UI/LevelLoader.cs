using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class LevelLoader : MonoBehaviour
{
	//public GameObject loadingScreen;
	//public UnityEngine.UI.Slider slider;
	//public Text progressText;
	[SerializeField]
	private string progressBarName = "loading-progress-bar";//loading-progress-bar
	[SerializeField]
	private UIDocument loadingUIDocument;
	private VisualElement loadingScreenVE;
	private ProgressBar progressBar;

	public void LoadLevel(int sceneIndex)
	{
		loadingScreenVE = loadingUIDocument.rootVisualElement;
		progressBar = loadingScreenVE.Q<ProgressBar>(progressBarName);
		StartCoroutine(LoadAsynchronously(sceneIndex));
	}

	IEnumerator LoadAsynchronously(int sceneIndex)
	{
		var operation = SceneManager.LoadSceneAsync(sceneIndex);

		progressBar.style.visibility = Visibility.Visible;

		while (!operation.isDone)
		{
			var progress = Mathf.Clamp01(operation.progress / .9f);

			progressBar.value = progress;

			yield return null;
		}
	}

	//IEnumerator OldLoadAsynchronously(int sceneIndex)
	//{
	//	var operation = SceneManager.LoadSceneAsync(sceneIndex);


	//	loadingScreen.SetActive(true);

	//	while (!operation.isDone)
	//	{
	//		var progress = Mathf.Clamp01(operation.progress / .9f);

	//		slider.value = progress;
	//		progressText.text = progress * 100f + "%";

	//		yield return null;
	//	}
	//}
}