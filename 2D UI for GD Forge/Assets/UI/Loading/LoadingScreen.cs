using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class LoadingScreen : VisualElement
{

    public new class UxmlFactory : UxmlFactory<LoadingScreen, UxmlTraits> { }

    public LoadingScreen()
    {
        this.RegisterCallback<GeometryChangedEvent>(OnGeometryChange);
    }

    void OnGeometryChange(GeometryChangedEvent evt)
    {
        this?.Q("start-game-button")?.RegisterCallback<ClickEvent>(ev => EnableNavigationScene());
        this.UnregisterCallback<GeometryChangedEvent>(OnGeometryChange);
    }

    public void EnableNavigationScene()
    {
        var loadingScript = GameObject.FindGameObjectWithTag("LoadingScript");
        loadingScript.GetComponent<LevelLoader>().LoadLevel(1);
    }
}