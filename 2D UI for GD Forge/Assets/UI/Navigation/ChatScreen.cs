using System;
using UnityEngine;
using UnityEngine.UIElements;

public class ChatScreen : VisualElement
{
    
    public new class UxmlFactory : UxmlFactory<ChatScreen, UxmlTraits> { }

    public ChatScreen()
    {
        this.RegisterCallback<GeometryChangedEvent>(OnGeometryChange);
    }

    void OnGeometryChange(GeometryChangedEvent evt)
    {
        this?.Q("chat-send-button")?.RegisterCallback<ClickEvent>(ev => SendMessageCall());


        this.UnregisterCallback<GeometryChangedEvent>(OnGeometryChange);
    }

    private void SendMessageCall()
    {
        var message = this?.Q<TextField>("chat-input-field").text;
        var messageScript = GameObject.FindGameObjectWithTag("ChatHandler");
        messageScript.GetComponent<ChatVisualizer>().SendMessage(message);
    }
}