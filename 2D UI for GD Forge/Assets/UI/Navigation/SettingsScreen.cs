using UnityEngine;
using UnityEngine.UIElements;

public class SettingsScreen : VisualElement
{

    public new class UxmlFactory : UxmlFactory<SettingsScreen, UxmlTraits> { }

    public SettingsScreen()
    {
        this.RegisterCallback<GeometryChangedEvent>(OnGeometryChange);
    }

    void OnGeometryChange(GeometryChangedEvent evt)
    {

        this?.Q("studio-link-button")?.RegisterCallback<ClickEvent>(ev => OpenFirstLink());
        this?.Q("another-studio-link-button")?.RegisterCallback<ClickEvent>(ev => OpenSecondLink());

        this.UnregisterCallback<GeometryChangedEvent>(OnGeometryChange);
    }

    public void OpenFirstLink()
    {
        Debug.Log("open first link");
        Application.OpenURL("http://gdforge.ru");
    }
    public void OpenSecondLink()
    {
        Debug.Log("open second link");
        Application.OpenURL("http://fairgames.studio");
    }
}