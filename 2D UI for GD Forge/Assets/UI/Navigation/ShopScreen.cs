using UnityEngine;
using UnityEngine.UIElements;

public class ShopScreen : VisualElement
{

    public new class UxmlFactory : UxmlFactory<ShopScreen, UxmlTraits> { }

    public ShopScreen()
    {
        this.RegisterCallback<GeometryChangedEvent>(OnGeometryChange);
    }

    void OnGeometryChange(GeometryChangedEvent evt)
    {


        this.UnregisterCallback<GeometryChangedEvent>(OnGeometryChange);
    }
}