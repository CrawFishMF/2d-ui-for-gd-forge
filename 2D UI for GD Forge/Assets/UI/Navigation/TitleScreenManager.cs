using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class TitleScreenManager : VisualElement
{
    VisualElement _TitleScreen;
    VisualElement _ChatScreen;
    VisualElement _SettingsScreen;
    VisualElement _ShopScreen;

    public new class UxmlFactory : UxmlFactory<TitleScreenManager, UxmlTraits> { }

    public TitleScreenManager()
    {
        this.RegisterCallback<GeometryChangedEvent>(OnGeometryChange);
    }

    void OnGeometryChange(GeometryChangedEvent evt)
    {
        _TitleScreen = this.Q("TitleScreen");
        _ChatScreen = this.Q("ChatScreen");
        _SettingsScreen = this.Q("SettingsScreen");
        _ShopScreen = this.Q("ShopScreen");

        this?.Q("shop-menu-button")?.RegisterCallback<ClickEvent>(ev => EnableShopScreen());
        this?.Q("chat-menu-button")?.RegisterCallback<ClickEvent>(ev => EnableChatScreen());
        this?.Q("settings-menu-button")?.RegisterCallback<ClickEvent>(ev => EnableSettingsScreen());
        this?.Q("main-menu-button")?.RegisterCallback<ClickEvent>(ev => EnableTitleScreen());

        this.UnregisterCallback<GeometryChangedEvent>(OnGeometryChange);
    }

    public void EnableShopScreen()
    {
        _TitleScreen.style.display = DisplayStyle.None;
        _ChatScreen.style.display = DisplayStyle.None;
        _SettingsScreen.style.display = DisplayStyle.None;
        _ShopScreen.style.display = DisplayStyle.Flex;
    }

    public void EnableChatScreen()
    {
        _TitleScreen.style.display = DisplayStyle.None;
        _ChatScreen.style.display = DisplayStyle.Flex;
        _SettingsScreen.style.display = DisplayStyle.None;
        _ShopScreen.style.display = DisplayStyle.None;
    }

    public void EnableSettingsScreen()
    {
        _TitleScreen.style.display = DisplayStyle.None;
        _ChatScreen.style.display = DisplayStyle.None;
        _SettingsScreen.style.display = DisplayStyle.Flex;
        _ShopScreen.style.display = DisplayStyle.None;
    }

    public void EnableTitleScreen()
    {
        _TitleScreen.style.display = DisplayStyle.Flex;
        _ChatScreen.style.display = DisplayStyle.None;
        _SettingsScreen.style.display = DisplayStyle.None;
        _ShopScreen.style.display = DisplayStyle.None;
    }

}
