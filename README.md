# README #

This project is about UI and logic around it. At first order UI maded for mobile screens (vertical) 

### About this repository ###

* This is test from [GD forge](http://gdforge.ru)
* Project were done by 5 days

### Assigned tasks ###

* Make loading screen
* Make Main-menu screen
* Make shop screen with list in grid-style
* Make settings screen with toggles and source link 
* Make Chat screen with visulization of sent messages

### Requirements ###

* Prototype must consider variability of UI
* Non-dynamically loaded content must be in Sprite Atlas
* Dynamic content must be loaded using the Addressables System